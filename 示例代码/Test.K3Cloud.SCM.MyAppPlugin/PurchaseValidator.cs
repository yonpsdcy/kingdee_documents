﻿using System;
using Kingdee.BOS;
using Kingdee.BOS.Core;
using Kingdee.BOS.Core.Validation;

namespace Test.K3Cloud.SCM.MyAppPlugin
{
    public class PurchaseValidator : AbstractValidator
    {
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="validateContext"></param>
        /// <param name="ctx"></param>
        public override void InitializeConfiguration(ValidateContext validateContext, Context ctx)
        {
            base.InitializeConfiguration(validateContext, ctx);

            if (validateContext.BusinessInfo != null)
            {
                EntityKey = validateContext.BusinessInfo.GetEntity(0).Key;
            }
        }

        /// <summary>
        /// 自定义校验
        /// </summary>
        /// <param name="dataEntities"></param>
        /// <param name="validateContext"></param>
        /// <param name="ctx"></param>
        public override void Validate(ExtendedDataEntity[] dataEntities, ValidateContext validateContext, Context ctx)
        {
            if (validateContext.IgnoreWarning)
            {
                return; //警告已经被用户忽略，就不需要再次执行了
            }

            if (dataEntities == null || dataEntities.Length <= 0)
            {
                return;
            }

            // 循环校验每一个数据包（一个数据包对应一张单据）
            foreach (var et in dataEntities)
            {
                // 订单编号在单据数据包中的字段名为：BillNo
                string billNo = Convert.ToString(et.DataEntity["BillNo"]);
                if (billNo.Length != 11)
                {
                    validateContext.AddError(et, new ValidationErrorInfo(
                       "BillNo",                                        // 出错的字段Key，可以空
                       Convert.ToString(et.DataEntity[0]),              // 数据包内码，必填，后续操作会据此内码避开此数据包
                       et.DataEntityIndex,                              // 出错的数据包在全部数据包中的顺序
                       et.RowIndex,                                     // 出错的数据行在全部数据行中的顺序，如果校验基于单据头，此为0
                       "Error 0",                                       // 错误编码，可以任意设定一个字符，主要用于追查错误来源
                       "数据校验未通过，单据编号必须是11个字符",        // 错误的详细提示信息
                       "单据合法性检查",                                // 错误的简明提示信息
                       ErrorLevel.FatalError                            // 错误级别
                    ));
                }
            }
        }
    }
}
