﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Kingdee.BOS;
using Kingdee.BOS.Core.DynamicForm;
using Kingdee.BOS.Core.DynamicForm.PlugIn;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Core.Interaction;
using Newtonsoft.Json.Linq;

namespace Test.K3Cloud.SCM.MyAppPlugin
{
    public class Class1: AbstractOperationServicePlugIn
    {
        // SpensorKey
        private const string SpensorKey = "DefaultSpensorKey";

        /// <summary>
        /// 单据校验。
        /// </summary>
        /// <param name="e"></param>
        public override void OnAddValidators(AddValidatorsEventArgs e)
        {
            base.OnAddValidators(e);

            var purchaseValidator = new PurchaseValidator();
            e.Validators.Add(purchaseValidator);    // 注册自定义校验器
        }

        public override void EndOperationTransaction(EndOperationTransactionArgs e)
        {
            base.EndOperationTransaction(e);

            // 当保存订单时
            if (FormOperation.OperationName == "保存")
            {
                /// 构造错误信息
                JArray errMsg = new JArray {
                    new JValue("01"),
                    new JValue("这是一条提示信息")
                };

                bool ignore = false;    // 窗口显示状态，默认不显示
                Option.TryGetVariableValue(SpensorKey, out ignore);
                if (!ignore && !Option.HasInteractionFlag(SpensorKey))
                {
                    KDInteractionException ie = ShowErrorMsg(Context, SpensorKey, ignore, errMsg);
                    throw ie;
                }
            }
        }

        /// <summary>
        /// 信息提示窗口
        /// </summary>
        /// <param name="context">上下文对象</param>
        /// <param name="spensorKey">窗口标识</param>
        /// <param name="ignore">状态</param>
        /// <param name="errorMsg">错误信息</param>
        /// <returns></returns>
        public static KDInteractionException ShowErrorMsg(Context context, string spensorKey, bool ignore, JArray errorMsg)
        {
            if (errorMsg.Count() != 2)
            {
                return null;
            }

            string titleMsg = "代码~|~信息";
            string errMsg = "{0}~|~{1}";
            K3DisplayerModel model = K3DisplayerModel.Create(context, titleMsg);
            model.AddMessage(string.Format(errMsg, errorMsg[0].ToString(), errorMsg[1].ToString()));
            model.Option.SetVariableValue(K3DisplayerModel.CST_FormTitle, "单据操作有以下信息出错，需要继续吗？");
            model.OKButton.Caption = new LocaleValue("是");
            model.CancelButton.Visible = model.OKButton.Visible = true;
            model.CancelButton.Caption = new LocaleValue("否");
            KDInteractionException ie = new KDInteractionException(spensorKey);
            ie.InteractionContext.InteractionFormId = "BOS_K3Displayer";
            ie.InteractionContext.K3DisplayerModel = model;
            ie.InteractionContext.IsInteractive = true;
            return ie;
        }
    }
}