﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Kingdee.BOS.Util;

namespace Test.K3Cloud.SCM.WebAPI.ServerExtend
{
    public class Checker
    {
        /// <summary>
        /// 检查客户端传递过来的参数。
        /// 客户端参数为JSON格式
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static Result clientParameterChecker(string parameters)
        {
            Result result = new Result();
            if (parameters == null)
            {
                result.msg = "接口请求参数不能为空";
                return result;
            }

            if (parameters.IsEmpty())
            {
                result.msg = "接口请求参数不能为空";
                return result;
            }

            result.success = true;
            return result;
        }

        /// <summary>
        /// 检查 Model
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static Result checkModel(Parameter parameter)
        {
            Result result = new Result();
            if (parameter.Model == null)
            {
                result.msg = "请求参数中固定参数 Model 不能为空";
                return result;
            }
            result.success = true;
            return result;
        }

        /// <summary>
        /// 检查业务参数中的 FID 和 FBillNo
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static Result checkFIDAndBillNo(Parameter parameter)
        {
            bool hasFID = false;
            bool hasFBillNo = false;
            if (parameter.Model.Property("FID") != null || Convert.ToInt64(parameter.Model["FID"]) > 0)
            {
                hasFID = true;
            }

            if (parameter.Model.Property("FBillNo") != null || Convert.ToString(parameter.Model["FBillNo"]).Length > 0)
            {
                hasFBillNo = true;
            }

            Result result = new Result();
            if (!hasFID && !hasFBillNo)
            {
                result.msg = "单据内码（FID）或者单据编码（FBillNo）不能同时为空";
                return result;
            }

            result.success = true;
            return result;
        }
    }
}
