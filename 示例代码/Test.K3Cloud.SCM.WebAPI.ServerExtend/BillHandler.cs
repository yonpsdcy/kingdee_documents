﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Kingdee.BOS;
using Kingdee.BOS.WebApi.FormService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Test.K3Cloud.SCM.WebAPI.ServerExtend
{
    public class BillHandler
    {
        /// <summary>
        /// 保存和更新
        /// </summary>
        /// <param name="context"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static Result Save(Context context, Parameter parameter)
        {
            // 更新
            var rs = WebApiServiceCall.Save(context, parameter.LocalFormId, Formater.Update(parameter));
            ResponseData res = Formater.ConvertWebAPIResult(rs);

            Result result = new Result();
            if (!res.Result.ResponseStatus.IsSuccess)
            {
                result.msg = JsonConvert.SerializeObject(res.Result.ResponseStatus.Errors) + "1";
                return result;
            }

            result.data = new JObject
            {
                new JProperty("FID", res.Result.ID),
                new JProperty("Number", res.Result.Number)
            };

            // 提交
            if (parameter.Model.Property("FID") == null && Convert.ToInt64(parameter.Model["FID"]) == 0)
            {
                parameter.Model.Add(new JProperty("FID", res.Result.ID));
            }

            if (parameter.Model.Property("FBillNo") == null && Convert.ToInt64(parameter.Model["FBillNo"]) == 0)
            {
                parameter.Model.Add(new JProperty("FBillNo", res.Result.Number));
            }

            var rsSubmit = WebApiServiceCall.Submit(context, parameter.LocalFormId, Formater.Submit(parameter));
            ResponseData rsSubmitData = Formater.ConvertWebAPIResult(rsSubmit);
            if (!rsSubmitData.Result.ResponseStatus.IsSuccess)
            {
                result.success = false;
                result.msg = JsonConvert.SerializeObject(rsSubmitData.Result.ResponseStatus.Errors) + "2";
                return result;
            }

            // 审核
            var rsAudit = WebApiServiceCall.Audit(context, parameter.LocalFormId, Formater.Audit(parameter));
            ResponseData rsAuditData = Formater.ConvertWebAPIResult(rsAudit);
            if (!rsAuditData.Result.ResponseStatus.IsSuccess)
            {
                result.success = false;
                result.msg = JsonConvert.SerializeObject(rsAuditData.Result.ResponseStatus.Errors) + "3";
                return result;
            }

            result.success = true;
            result.msg = Defineds.DefaultSuccessMsg;
            return result;
        }
    }
}
