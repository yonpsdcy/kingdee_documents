﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json.Linq;

namespace Test.K3Cloud.SCM.WebAPI.ServerExtend
{
    public class Defineds
    {
        public const string DefaultSuccessMsg = "星空系统单据处理成功";
    }

    /// <summary>
    /// API返回结果对象
    /// </summary>
    public struct Result
    {
        public bool success;    // 请求执行状态，成功：true；失败：false；
        public string msg;      // 返回描述信息
        public JObject data;    // 请求返回的数据
    }

    /// <summary>
    /// API请求参数的结构体
    /// </summary>
    public struct Parameter
    {
        public string LocalFormId;          // 上游单据ID
        public string EntryKey;             // 下游单据明细单据单据ID
        public JObject Model;               // 具体参数
    }

    /// <summary>
    /// 保存接口请求参数
    /// </summary>
    public struct RequestSave
    {
        public JArray NeedUpDateFields;         // 需要更新的字段，数组类型，格式：[key1,key2,...] （非必录）注（更新单据体字段得加上单据体key
        public JArray NeedReturnFields;         // 需返回结果的字段集合，数组类型，格式：[key,entitykey.key,...]（非必录） 注（返回单据体字段格式：entitykey.key）
        public bool IsDeleteEntry;              // 是否删除已存在的分录，布尔类型，默认true（非必录）
        public string SubSystemId;              // 表单所在的子系统内码，字符串类型（非必录）
        public bool IsVerifyBaseDataField;      // 是否验证所有的基础资料有效性，布尔类，默认false（非必录）
        public bool IsEntryBatchFill;           // 是否批量填充分录，默认true（非必录）
        public bool ValidateFlag;               // 是否验证标志，布尔类型，默认true（非必录）
        public bool NumberSearch;               // 是否用编码搜索基础资料，布尔类型，默认true（非必录）
        public string InterationFlags;          // 交互标志集合，字符串类型，分号分隔，格式："flag1;flag2;..."（非必录） 例如（允许负库存标识：STK_InvCheckResult）
        public JObject Model;                    // 表单数据包，JSON类型（必录）
    }

    /// <summary>
    /// 提交接口请求参数
    /// </summary>
    public struct RequestSubmit
    {
        public long CreateOrgId;           // 创建者组织内码，字符串类型（非必录）
        public JArray Numbers;              // 单据编码集合，数组类型，格式：[No1,No2,...]（使用编码时必录）
        public string Ids;                  // 单据内码集合，字符串类型，格式："Id1,Id2,..."（使用内码时必录）
        public long SelectedPostId;        // 工作流发起员工岗位内码，整型（非必录） 注（员工身兼多岗时不传参默认取第一个岗位）
        public bool NetworkCtrl;            // 是否启用网控，布尔类型，默认false（非必录）
    }

    /// <summary>
    /// 审核接口请求参数
    /// </summary>
    public struct RequestAudit
    {
        public long CreateOrgId;           // 创建者组织内码，字符串类型（非必录）
        public JArray Numbers;              // 单据编码集合，数组类型，格式：[No1,No2,...]（使用编码时必录）
        public string Ids;                  // 单据内码集合，字符串类型，格式："Id1,Id2,..."（使用内码时必录）
        public string InterationFlags;      // 交互标志集合，字符串类型，分号分隔，格式："flag1;flag2;..."（非必录） 例如（允许负库存标识：STK_InvCheckResult）
        public bool NetworkCtrl;            // 是否启用网控，布尔类型，默认false（非必录）
    }

    /// <summary>
    /// 接口返回数据
    /// </summary>
    public struct ResponseData
    {
        public ResponseDataResult Result;
    }

    /// <summary>
    /// 返回数据结构
    /// </summary>
    public struct ResponseDataResult
    {
        public ResponseDataResultStatus ResponseStatus;
        public long ID;                    // 单据内码
        public string Number;               // 单据编号
        public JArray NeedReturnData;       // 返回结果的字段集合
    }

    /// <summary>
    /// 返回结果状态
    /// </summary>
    public struct ResponseDataResultStatus
    {
        public bool IsSuccess;          // 是否操作成功
        public long ErrorCode;
        public List<ResponseDataResultErrors> Errors;           // 失败错误信息
        public List<ResponseDataResultSuccessEntitys> SuccessEntitys;  // 操作成功的实体信息
        public List<ResponseDataResultSuccessMessages> SuccessMessages;  // 成功消息
        public long MsgCode;             // 消息代码
    }

    /// <summary>
    /// 返回结果中错误信息结构
    /// </summary>
    public struct ResponseDataResultErrors
    {
        public string FieldName;
        public string Message;
        public long DIndex;
    }

    /// <summary>
    /// 返回结果中成功的明细
    /// </summary>
    public struct ResponseDataResultSuccessEntitys
    {
        public long Id;
        public string Number;
        public long DIndex;
    }

    /// <summary>
    /// 返回结果中成功的信息
    /// </summary>
    public struct ResponseDataResultSuccessMessages
    {
        public string FieldName;
        public string Message;
        public long DIndex;
    }

    /// <summary>
    /// 返回结果，包含执行状态
    /// </summary>
    public struct ResponseDataResultConvertStatus
    {
        public bool IsSuccess;          // 是否操作成功
        public long ErrorCode;
        public List<ResponseDataResultErrors> Errors;           // 失败错误信息
        public List<ResponseDataResultSuccessEntitys> SuccessEntitys;  // 操作成功的实体信息
        public List<ResponseDataResultSuccessMessages> SuccessMessages;  // 成功消息
        public long MsgCode;             // 消息代码
    }
}
