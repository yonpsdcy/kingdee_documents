﻿## 星空系统自定义WebAPI示例代码

## 介绍
本源码是"[金蝶云星空插件实战开发-新手入门教程-自定义WebAPI](https://mp.weixin.qq.com/s/DMD4ayo2FeALa-SEclwChQ)"的示例代码。  
大家可以跟着教程和源码来学习和理解星空系统自定义WebAPI的开发。不建议用于实际项目中。  
