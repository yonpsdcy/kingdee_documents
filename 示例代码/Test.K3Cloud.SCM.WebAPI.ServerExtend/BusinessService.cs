﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Kingdee.BOS;
using Kingdee.BOS.ServiceFacade.KDServiceFx;
using Kingdee.BOS.WebApi.ServicesStub;
using Newtonsoft.Json;

namespace Test.K3Cloud.SCM.WebAPI.ServerExtend
{
    public class BusinessService : AbstractWebApiBusinessService
    {
        public BusinessService(KDServiceContext context) : base(context) { }

        /// <summary>
        /// 执行接口请求
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public Result ExcuteService(string parameters)
        {
            parameters = parameters.Trim();
            Result result = Checker.clientParameterChecker(parameters);
            if (!result.success)
            {
                return result;
            }

            return ServiceRouter(parameters);
        }

        /// <summary>
        /// 接口请求分发
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private Result ServiceRouter(string parameters)
        {
            // 反序列化客户端传递的JSON数据
            Parameter parameter = JsonConvert.DeserializeObject<Parameter>(parameters);
            Result checkRs = Checker.checkModel(parameter); // 业务参数不能为空
            if (!checkRs.success)
            {
                return checkRs;
            }

            Context context = KDContext.Session.AppContext;
            return BillHandler.Save(context, parameter);
        }
    }
}