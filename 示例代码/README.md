# 示例代码

### 介绍
本仓库主要是用于存放发布于公众号【代码乾坤(CoderLand)】和[知乎](https://www.zhihu.com/column/c_1408920220563734528)等平台的教程的源代码。

#### 单据插件代码
Test.K3Cloud.SCM.MyPlugin  
[金蝶云星空插件实战开发-新手入门教程-表单插件](http://mp.weixin.qq.com/s?__biz=MzkwNTIzNDE1OA==&mid=2247483693&idx=1&sn=ed3612165252405bbd8dc422e6e45bb4&chksm=c0fb92acf78c1bbaf58116faae3b7e94cd4a0c9fe95fd0d046b57f8e8d895903ea5b7bfe17c9&scene=21#wechat_redirect)  

Test.K3Cloud.SCM.MyListPlugin  
[金蝶云星空插件实战开发-新手入门教程-列表插件](http://mp.weixin.qq.com/s?__biz=MzkwNTIzNDE1OA==&mid=2247483723&idx=1&sn=80526e77a8cbca660fae9195e20a6e65&chksm=c0fb92caf78c1bdcf6e96656c0b3aaceb841df0d4983ab4773eeea249329ceedfbbd43bd38ce&scene=21#wechat_redirect)

Test.K3Cloud.SCM.MyAppPlugin  
[金蝶云星空插件实战开发-新手入门教程-服务插件](https://mp.weixin.qq.com/s/1MHDjC1a9xnakesQU2qRug)

Test.K3Cloud.SCM.WebAPI.ServerExtend  
[金蝶云星空插件实战开发-新手入门教程-自定义WebAPI](https://mp.weixin.qq.com/s/DMD4ayo2FeALa-SEclwChQ)