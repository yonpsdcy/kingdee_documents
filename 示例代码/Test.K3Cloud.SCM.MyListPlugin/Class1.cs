﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Kingdee.BOS.Core.DynamicForm;
using Kingdee.BOS.Core.DynamicForm.PlugIn.Args;
using Kingdee.BOS.Core.List.PlugIn;

namespace Test.K3Cloud.SCM.MyListPlugin
{
    public class Class1: AbstractListPlugIn
    {
        /// <summary>
        /// 工具条菜单点击事件
        /// </summary>
        /// <param name="e"></param>
        public override void BarItemClick(BarItemClickEventArgs e)
        {
            base.BarItemClick(e);
            // Test_tbButton 为自定义按钮的标识
            if (e.BarItemKey == "Test_tbButton")
            {
                View.ShowMessage("Hello world!", MessageBoxType.Notice);
            }
        }
    }
}
