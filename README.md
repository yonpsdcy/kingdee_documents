# Kingdee_documents

#### 介绍
本仓库主要是用于整理和收集金蝶云星空二次开发相关文档，供开发者参考。 

金蝶云星空系统二次开发资料相对分散，开发者在学习和开发时为了解决问题需要花费不少的精力去寻找文档和资料，本项目尽力将文档和帖子综合在一起。

#### 金蝶云.星空系统二次开发基础介绍
星空系统的二次开发主要分为以下几个部分的相互配合来达到对星空系统的增强：
1. BOS开发
2. BOS插件开发
3. WebAPI开发

金蝶云BOS的全称是Business Operation System(业务操作系统)，是针对金蝶云产品，自主研发的新一代技术平台。其核心技术是动态领域建模。基于动态领域建模技术， BOS平台抽象了动态表单、业务单据、基础资料、业务流程、工作流、报表、弹性域、权限、多组织架构模型和框架，提供了元数据驱动架构、一系列业务模型元数据解释引擎和运行时业务服务构件，面向业务人员领域建模，快速搭建复杂的、可扩展的商业应用，满足企业未来发展和变化的应用需要，为客户带来持续的信息化投资收益。BOS平台还提供了一套完整的业务建模、插件开发、系统测试、运行部署的解决方案，允许伙伴、独立软件开发商、客户聚焦于高价值的应用，可以以最小的代价基于BOS平台开发行业扩展应用，也可开发独立的应用系统，而无需关注技术细节。  

BOS平台提供动态扩展功能，通过动态领域模型建模将业务成各种标准接口和标准服务，通过插件 开发完成标准业务扩展，快速高效实现个性化和非标准化业务。  
BOS平台的插件是一种专门为某一软件设计、用于功能扩充的程序。   
通过它可以： 
1. 实现更灵活的控制方式和客户化定制；
2. 实现标准产品中特殊的业务流程处理；
3. 进行个性化的菜单、外观定制；
4. 增加自定义的菜单并响应其操作；
5. 对已有操作和服务未支持的功能进行扩展；
6. 快速开发、快速实施、快速应用；
7. 可配置，可测试，快速部署。 

BOS平台提供了4类插件：
1. 动态表单插件   
   业务逻辑封装的插件，用于单据上业务各种事件、操作和服务的介入；  
   提供了一系列事件、操作和服务接口；   
   此类插件最常用。   
2. 服务插件   
   基于动态表单服务端的插件，提供服务编程接口；  
   此类接口在外部服务直接调用。 
3. 校验规则插件   
   针对业务规则进行校验的插件； 
4. 表单构建插件  
   对界面加载控件和元数据的插件；   
   通过此插件可对界面控件及元数据动态调整实现特殊需求。

WebAPI 为异构系统访问 K/3Cloud 系统数据提供通用的接口。WebAPI是一种轻量级的、可维护的、可伸缩的 Web 服务。采用 HTTP+JSON，也就是用RESTful的方式来开发。使用 .NET Framework 4.0 为开发平台，源代码使用 C#编写。整个框架由 三个组装件组成。
WebAPI提供了以下能力：  
1. 提供对 K/3Cloud 单据和基础信息的查看、保存、提交、审核、反审核和删除等功能；
2. 提供对 K/3Cloud 单据和基础信息的查询功能；

但是WebAPI有以下约束：
1. 数据操作接口仅支持以基础资料编码、单据编号或直接以表单主键去操作数据；
2. 支持对某一具体单据的数据查询，但多单关联查询需要二开接口实现。  

WebAPI官方文档【重要】：https://openapi.open.kingdee.com/ApiHome

#### 项目目录
├─ [根目录](https://gitee.com/hsg4ok_admin/kingdee_documents/tree/master)  
├── [文档汇总(Readme)](https://gitee.com/hsg4ok_admin/kingdee_documents/blob/master/README.md)  
├── [星空系统文档收集](https://gitee.com/hsg4ok_admin/kingdee_documents/tree/master/%E6%98%9F%E7%A9%BA%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3%E6%94%B6%E9%9B%86)  
├──── [BOS](https://gitee.com/hsg4ok_admin/kingdee_documents/tree/master/%E6%98%9F%E7%A9%BA%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3%E6%94%B6%E9%9B%86/BOS)  
├──── [插件](https://gitee.com/hsg4ok_admin/kingdee_documents/tree/master/%E6%98%9F%E7%A9%BA%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3%E6%94%B6%E9%9B%86/%E6%8F%92%E4%BB%B6)  
├──── [系统集成](https://gitee.com/hsg4ok_admin/kingdee_documents/tree/master/%E6%98%9F%E7%A9%BA%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3%E6%94%B6%E9%9B%86/%E7%B3%BB%E7%BB%9F%E9%9B%86%E6%88%90)  
├──── [综合](https://gitee.com/hsg4ok_admin/kingdee_documents/tree/master/%E6%98%9F%E7%A9%BA%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3%E6%94%B6%E9%9B%86/%E7%BB%BC%E5%90%88)  
├──── [实验教材.pdf](https://gitee.com/hsg4ok_admin/kingdee_documents/blob/master/%E6%98%9F%E7%A9%BA%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3%E6%94%B6%E9%9B%86/K3Cloud%E6%99%BA%E9%80%A0%E6%9C%8D%E5%8A%A1%E5%B9%B3%E5%8F%B0V3.0_%E9%99%A2%E6%A0%A1%E7%89%88_%E5%AE%9E%E9%AA%8C%E6%95%99%E6%9D%90_BOS.pdf)  

### 金蝶云.星空 - 二次开发知识地图
#### 安装部署
请在官网下载最新版本安装包+最新补丁包，参考安装包help目录下的产品安装指南。  
金蝶云星空开放平台的官网地址是：https://open.kingdee.com  
安装问题排除参考：https://vip.kingdee.com/article/45669

#### 开发规范
[开发规范【重要】](https://vip.kingdee.com/article/3205)

#### BOS IDE入门
[基本配置手册及常用操作说明【基础】](https://vip.kingdee.com/article/9140)  
[BOS设计器【入门】](https://vip.kingdee.com/article/57854870606429952)  
[扩展业务对象【入门】](https://open.kingdee.com/K3Cloud/WenKu/DocumentView.aspx?docId=116204)  
[新建业务对象【入门】](http://open.kingdee.com/K3Cloud/WenKu/DocumentView.aspx?docId=116205)  
[权限控制【入门】](https://vip.kingdee.com/school/1062)  
[BOS IDE案例【视频】](https://vip.kingdee.com/tolink?target=https%3A%2F%2Fpan.baidu.com%2Fs%2F1i39t9BJ)  
[BOS平台院校版教材【入门】](https://vip.kingdee.com/school/1047)  

#### 常用功能
[编码规则【重要】](https://vip.kingdee.com/school/1048)  
[附件及文件服务器](https://vip.kingdee.com/article/9142)  
[值更新/实体服务](https://vip.kingdee.com/article/3214)  
[网络控制](https://vip.kingdee.com/school/1049)  
[业务配置](https://vip.kingdee.com/school/1050)  
[自动补号重构说明](https://vip.kingdee.com/article/3222)  
[发送邮件](https://vip.kingdee.com/article/3223)  
[系统预设快捷键](https://vip.kingdee.com/article/3225)  
[上机操作日志丢失排查](https://vip.kingdee.com/article/3224)  
[Win10环境兼容问题解决方法](https://vip.kingdee.com/article/9143)  

#### 帐表
[报表平台【入门】](https://vip.kingdee.com/article/57859118044126209)  
[简单帐表](https://vip.kingdee.com/article/9144)  
[分页帐表](https://vip.kingdee.com/article/3229)  
[直接SQL帐表](https://vip.kingdee.com/article/9145)  
[透视表](https://vip.kingdee.com/article/3232)  
[帐表开发【汇总贴】](https://vip.kingdee.com/article/9147)  
[账表服务取数插件示例代码](https://vip.kingdee.com/school/1051)  

#### 万能报表
[万能报表设计器用户手册](https://vip.kingdee.com/article/9148)  
[万能报表ICOM培训【视频】](https://vip.kingdee.com/school/1052)  
[常见案例](https://vip.kingdee.com/article/9149)  

#### 部署/发布/解决方案
[解决方案](https://vip.kingdee.com/school/1053)  
[部署](https://vip.kingdee.com/school/1054)  
[发布](https://vip.kingdee.com/school/1055)  
[开发部署行为规范【重要】](https://vip.kingdee.com/article/31442)

#### 套打
[套打平台【汇总贴】](https://vip.kingdee.com/article/57858307285163265)  
[凭证打印从入门到进阶【汇总贴】](https://vip.kingdee.com/article/55218864090863105)  
[操作手册【基础】](https://vip.kingdee.com/article/9140)  
[套打模板操作案例【视频】](https://vip.kingdee.com/tolink?target=http%3A%2F%2Fpan.baidu.com%2Fs%2F1jGKMOr8)  
[常见问题](https://vip.kingdee.com/article/9150)  
[常见问题与应用](https://vip.kingdee.com/article/148489)  

#### 工作流
[工作流和审批流区别](https://vip.kingdee.com/school/1056)  
[工作流相关知识文档【汇总贴】](https://vip.kingdee.com/article/9158)

#### 业务流
[业务流程汇总(包括单据转换、反写规则)【汇总贴】](https://vip.kingdee.com/article/22473)

#### 移动平台
[移动BOS平台【汇总贴】](https://vip.kingdee.com/article/178089152616458496)  
[移动平台开发【入门】](http://club.kingdee.com/forum.php?mod=viewthread&tid=995906)  
[移动开发知识【汇总贴】](https://vip.kingdee.com/article/4872)

#### 插件开发 
[从零开始开发插件【入门】](https://vip.kingdee.com/article/83500607104976896)  
[二次开发开规范部署【入门】](https://vip.kingdee.com/article/2225)   
[二次开发案例演示【汇总】【入门】【重要】](https://vip.kingdee.com/article/94751030918525696)   
[数据模型【基础】](http://open.kingdee.com/K3Cloud/CDPPortal/DataModel.aspx)  
[BOS二次开发示例【汇总贴】](https://vip.kingdee.com/article/3906)  
[流程插件开发](https://vip.kingdee.com/article/9174)  
[二次开发基于WebDev附加进程调试【入门】](https://vip.kingdee.com/article/3904)  
[DynamicObject的结构及操作【入门】](https://vip.kingdee.com/article/2591)  
[弹性域](https://vip.kingdee.com/article/9177)  
[不重启IIS开发插件【重要】](https://vip.kingdee.com/article/1361)  

#### 系统集成
[系统集成使用指南【汇总贴】](https://vip.kingdee.com/article/76278025062688512)  
[K/3 Cloud系统集成【汇总贴】](https://vip.kingdee.com/article/9185) 

#### 执行计划
[执行计划](https://vip.kingdee.com/article/9178) 

#### 常见案例
[Python案例【汇总贴】](https://vip.kingdee.com/article/9183) 

#### 其他
[BOS系统对象手册【重要】【高级】](https://open.kingdee.com/K3Cloud/SDK/webframe.html)  
[技术答疑.常用SQL](https://vip.kingdee.com/article/146201387037720320)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
